
;(function($) {

	$('.bonita-tab-nav a').on('click',function (e) {
		e.preventDefault();
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('.bonita-tab-nav .begin').on('click',function (e) {		
		$('.bonita-tab-wrapper .begin').addClass('show').siblings().removeClass('show');
	});	
	$('.bonita-tab-nav .actions, .bonita-tab .actions').on('click',function (e) {		
		e.preventDefault();
		$('.bonita-tab-wrapper .actions').addClass('show').siblings().removeClass('show');

		$('.bonita-tab-nav a.actions').addClass('active').siblings().removeClass('active');

	});	
	$('.bonita-tab-nav .support').on('click',function (e) {		
		$('.bonita-tab-wrapper .support').addClass('show').siblings().removeClass('show');
	});	
	$('.bonita-tab-nav .table').on('click',function (e) {		
		$('.bonita-tab-wrapper .table').addClass('show').siblings().removeClass('show');
	});	

})(jQuery);
