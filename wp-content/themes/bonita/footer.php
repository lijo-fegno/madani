<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Bonita
 */
?>
			</div>
		</div>
	</div><!-- #content -->

	<?php do_action('bonita_before_footer'); ?>

	<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
		<?php get_sidebar('footer'); ?>
	<?php endif; ?>

    <a class="go-top"><i class="bonita-svg-icon"><?php bonita_get_svg_icon( 'icon-chevron-up', true ); ?></i></a>
		
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<p>2020 Copyright Designed by Love. All Rights Reserved.</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<?php do_action('bonita_after_footer'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
